from django.shortcuts import render
from django.contrib.contenttypes.models import ContentType
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

from like.models import LikeCount, LikeRecord
#引入权限检查模块
from django.contrib.auth.models import User
from django.contrib.auth.models import Permission


def success_response(likes_num, is_liked):
    data = {'status': 'SUCCESS', 'likes_num': likes_num, 'is_liked': is_liked}
    return JsonResponse(data)


def error_response(code, message):
    data = {'status': 'ERROR', 'code': code, 'message': message}
    return JsonResponse(data)


@login_required(login_url='/userprofile/login/')
def like_change(request):
    # 判断该用户是否有点赞权限
    myuser = User.objects.get(id=request.user.id)
    if not myuser.has_perm('like.change_likecount'):
        return HttpResponse('抱歉，您没有点赞的权限；如有疑问，请联系管理员！！！')
    # 获取数据和对应的对象
    user = request.user
    content_type = request.GET.get('content_type')
    content_type = ContentType.objects.get(model=content_type)
    object_id = request.GET.get('object_id')

    like_record, created = LikeRecord.objects.get_or_create(content_type=content_type, object_id=object_id, user=user)
    if created:
        # 未点赞过，进行点赞，创建点赞记录
        like_count, created = LikeCount.objects.get_or_create(content_type=content_type, object_id=object_id)
        like_count.likes_num += 1
        like_count.save()
        return success_response(like_count.likes_num, True)
    else:
        # 已点赞过，取消点赞，删除点赞记录
        like_record = LikeRecord.objects.get(content_type=content_type, object_id=object_id, user=user)
        like_record.delete()
        like_count, created = LikeCount.objects.get_or_create(content_type=content_type, object_id=object_id)
        if not created:
            like_count.likes_num -= 1
            like_count.save()
            return success_response(like_count.likes_num, False)
        else:
            return error_response('404', 'data error')
