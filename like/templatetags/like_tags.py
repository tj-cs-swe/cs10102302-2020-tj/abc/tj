from django import template
from django.contrib.contenttypes.models import ContentType
from ..models import LikeCount, LikeRecord
from article.models import ArticlePost
from question.models import QuestionPost
from answer.models import Answer
from comment.models import Comment

register = template.Library()

@register.filter(name='add_arg')
def template_args(instance, arg):
    """stores the arguments in a separate instance attribute"""
    if not hasattr(instance, "_TemplateArgs"):
        setattr(instance, "_TemplateArgs", [])
    instance._TemplateArgs.append(arg)
    return instance


@register.filter(name='call')
def template_method(instance, method):
    """retrieves the arguments if any and calls the method"""
    method = getattr(instance, method)
    if hasattr(instance, "_TemplateArgs"):
        to_return = method(*instance._TemplateArgs)
        delattr(instance, '_TemplateArgs')
        return to_return
    return method()

# 得到点赞数量
@register.simple_tag
def get_likes_num(obj):
    content_type = ContentType.objects.get_for_model(obj)
    like_count, created = LikeCount.objects.get_or_create(content_type=content_type, object_id=obj.id)
    return like_count.likes_num

