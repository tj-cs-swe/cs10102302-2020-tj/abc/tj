from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.auth.models import User


class CollectCount(models.Model):
    # 内容类型
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    # id
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("content_type", "object_id")

    collect_num = models.PositiveIntegerField(default=0)


class CollectRecord(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("content_type", "object_id")

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    collected_time = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-collected_time']

