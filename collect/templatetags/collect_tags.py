#!/usr/bin/env python 
# -*- coding:utf-8 -*-
from django import template
from django.contrib.contenttypes.models import ContentType
from ..models import CollectCount, CollectRecord
from article.models import ArticlePost
from question.models import QuestionPost

register = template.Library()


@register.filter(name='add_arg')
def template_args(instance, arg):
    """stores the arguments in a separate instance attribute"""
    if not hasattr(instance, "_TemplateArgs"):
        setattr(instance, "_TemplateArgs", [])
    instance._TemplateArgs.append(arg)
    return instance


@register.filter(name='call')
def template_method(instance, method):
    """retrieves the arguments if any and calls the method"""
    method = getattr(instance, method)
    if hasattr(instance, "_TemplateArgs"):
        to_return = method(*instance._TemplateArgs)
        delattr(instance, '_TemplateArgs')
        return to_return
    return method()


# 得到收藏数量
@register.simple_tag
def get_collect_num(obj):
    content_type = ContentType.objects.get_for_model(obj)
    collect_count, created = CollectCount.objects.get_or_create(content_type=content_type, object_id=obj.id)
    return collect_count.collect_num
