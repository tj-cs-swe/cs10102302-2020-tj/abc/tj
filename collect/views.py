from django.shortcuts import render
from django.contrib.contenttypes.models import ContentType
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

from collect.models import CollectCount, CollectRecord
#引入权限检查模块
from django.contrib.auth.models import User
from django.contrib.auth.models import Permission


def success_response(collect_num, is_collected):
    data = {'status': 'SUCCESS', 'collect_num': collect_num, 'is_collected': is_collected}
    return JsonResponse(data)


def error_response(code, message):
    data = {'status': 'ERROR', 'code': code, 'message': message}
    return JsonResponse(data)


@login_required(login_url='/userprofile/login/')
def collect_change(request):
    # 判断该用户是否有收藏权限
    myuser = User.objects.get(id=request.user.id)
    if not myuser.has_perm('collect.change_collectcount'):
        return HttpResponse('抱歉，您没有收藏的权限；如有疑问，请联系管理员！！！')
    # 获取数据和对应的对象
    user = request.user
    content_type = request.GET.get('content_type')
    content_type = ContentType.objects.get(model=content_type)
    object_id = request.GET.get('object_id')

    collect_record, created = CollectRecord.objects.get_or_create(content_type=content_type, object_id=object_id, user=user)
    if created:
        # 未收藏过，进行收藏，创建收藏记录
        collect_count, created = CollectCount.objects.get_or_create(content_type=content_type, object_id=object_id)
        collect_count.collect_num += 1
        collect_count.save()
        return success_response(collect_count.collect_num, True)
    else:
        # 已收藏过，取消收藏，删除收藏记录
        collect_record = CollectRecord.objects.get(content_type=content_type, object_id=object_id, user=user)
        collect_record.delete()
        collect_count, created = CollectCount.objects.get_or_create(content_type=content_type, object_id=object_id)
        if not created:
            collect_count.collect_num -= 1
            collect_count.save()
            return success_response(collect_count.collect_num, False)
        else:
            return error_response('404', 'data error')