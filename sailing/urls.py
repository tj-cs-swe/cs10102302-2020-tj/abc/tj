"""sailing URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.shortcuts import render
from article import views as article_views
from question import views as question_views
from userprofile import views as user_views

from django.conf import settings
from django.conf.urls.static import static
from itertools import chain
import notifications.urls
def mainPage(request):
    return render(request, 'userprofile/collection.html', {})


def sort_column(column_questions, column_articles):
    column_content = []

    for question in column_questions:
        content = {'title': question.title, 'id': question.id, 'column': str(question.column), 'updated': question.updated, 'type': 'question'}
        column_content.append(content)
        pass

    for article in column_articles:
        content = {'title': article.title, 'id': article.id, 'column': str(article.column), 'updated': article.updated, 'type': 'article'}
        column_content.append(content)
        pass

    column_content = sorted(column_content, key=lambda x:(x['column'], x['updated']), reverse = True)
    
    #for item in column_content:
    #    if item['column'] != 'None':
    #        print(item)
    return column_content

def hot_content(request):
    # hotContent
    hot_questions = question_views.hot_question_list()
    hot_articles = article_views.hot_article_list()
    # blockContent
    column_questions = question_views.column_question_list()
    column_articles = article_views.column_article_list()
    column_content = sort_column(column_questions["column_questions"], column_articles["column_articles"])
    column_content = {'column_content': column_content}

    context = {**hot_questions, **hot_articles, **column_content}
    return render(request, 'mainPage.html', context)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('userprofile/', include('userprofile.urls', namespace='userprofile')),
    path('article/', include('article.urls', namespace='article')),
    path('question/', include('question.urls', namespace='question')),
    path('comment/', include('comment.urls', namespace='comment')),
    path('answer/', include('answer.urls', namespace='answer')),
    path('like/', include('like.urls', namespace='like')),
    path('', hot_content, name='hot_content'),
    path('collect/', include('collect.urls', namespace='collect')),
    path('search_question/', question_views.search_question),
    path('search_article/', article_views.search_article),
    path('search_user/', user_views.search_user),
    path('blocks/',include('blocks.urls', namespace='blocks')),
    path('inbox/notifications/', include(notifications.urls, namespace='notifications')),
    path('notice/', include('notice.urls', namespace='notice')),
    # path('', mainPage)
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)