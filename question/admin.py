from django.contrib import admin
from .models import QuestionPost, QuestionColumn

# Register your models here.

admin.site.register(QuestionPost)

admin.site.register(QuestionColumn)
