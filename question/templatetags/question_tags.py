from django import template

register = template.Library()


# @register.filter('list')
# def do_list(value):
#     return range(0, value)  # 未发现在项目中使用

# 增加自定义过滤器button_type，用于判断每一个文章/问题所属板块类型
@register.filter('button_type')
def button_type(name: str) -> str:
    if name == "生活":
        return "btn-warning"
    elif name == "学习":
        return "btn-danger"
    elif name == "交友":
        return "btn-success"
    else:
        return ""
