from django import forms
from .models import QuestionPost


# 发布问题的表单类
class QuestionPostForm(forms.ModelForm):
    class Meta:
        # 指明数据模型来源
        model = QuestionPost
        # 定义表单包含的字段:问题属于的分区，标题，描述
        fields = ('title', 'description', 'file')
