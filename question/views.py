import os
import markdown

from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpResponse, StreamingHttpResponse
from django.conf import settings
from django.utils.http import urlquote
from django.db.models import Q  # 引入 Q 对象，进行组合查询
from django.core.paginator import Paginator  # 引入分页模块
from django.utils.html import strip_tags  # 去掉所有html标签

from .models import QuestionPost, QuestionColumn
from .forms import QuestionPostForm
from answer.models import Answer


def search_question(request):
    """
    搜索问题视图
    存在搜索内容，则返回与内容相关的问题，否则返回所有的问题(分页形式)
    """
    search = request.GET.get('search')  # 获取前端发送的搜索内容
    # 如果进行了搜索
    if search:
        questions = QuestionPost.objects.filter(
            # 匹配标题(i不区分大小写)
            Q(title__icontains=search) |
            # 匹配问题描述(不区分大小写)
            Q(description__icontains=search)
        ).order_by('-updated')
    # 如果未进行搜索
    else:
        # 获取所有问题
        question_list = QuestionPost.objects.all()
        # 每页显示 5 篇文章
        paginator = Paginator(question_list, 5)
        # 获取 url 中的页码
        page = request.GET.get('page')
        # 将导航对象相应的页码内容返回给questions
        questions = paginator.get_page(page)
    context = {'questions': questions, 'search_q': search}
    return render(request, 'search/search_question.html', context)


def hot_question_list():
    """
    获取热门问题函数，
    先按照总浏览量，后按照更新时间排序
    """
    questions = QuestionPost.objects.order_by('-total_views', '-updated')[:5]
    context = {'questions': questions}
    return context


def column_question_list():
    """
    获取板块问题函数，
    先按照板块(生活、学习、交友)，后按照更新时间排序
    """
    questions = QuestionPost.objects.all().order_by('column', '-updated')
    context = {'column_questions': questions}
    return context


def user_has_permission(user_id: int, perm_name: str) -> bool:
    """
    判断用户权限函数
    根据传入的用户名和权限名称，返回该用户是否拥有该权限的bool变量
    """
    cur_user = User.objects.get(id=user_id)
    return cur_user.has_perm(perm_name)


def question_detail(request, question_id):
    """
    问题详情视图
    根据传入的问题id，返回该问题的详情界面
    """
    question = QuestionPost.objects.get(id=question_id)
    # 更新浏览量
    question.total_views += 1
    # 保存新的浏览量
    question.save(update_fields=['total_views'])
    question.description = strip_tags(question.description)
    # 包含缩写、表格等常用扩展 和 语法高亮扩展
    question.description = markdown.markdown(question.description,
                                             extensions=['markdown.extensions.extra',
                                                         'markdown.extensions.codehilite'])
    # 获取该问题的所有回答
    answers = Answer.objects.filter(question=question_id)
    context = {'question': question, 'answers': answers}
    return render(request, 'question/detail.html', context)


@login_required(login_url='/userprofile/login/')
def question_create(request):
    """
    发布问题视图

    判断 权限 后，
    通过POST提交创建问题，返回问题详情界面，
    GET方式进入创建问题界面
    """
    # 判断该用户是否有相关权限
    if not user_has_permission(request.user.id, 'question.add_questionpost'):
        return HttpResponse('抱歉，您的相关权限已被管理员封禁；如有疑问，请联系管理员！！！')

    is_initial = True  # 标识用户创建问题

    # 判断用户是否提交数据
    if request.method == "POST":
        # 将提交的数据赋值到表单实例
        question_post_form = QuestionPostForm(request.POST, request.FILES)
        # 判断提交的数据是否满足要求
        if question_post_form.is_valid():
            # 保存数据，但暂时不提交到数据库
            new_question = question_post_form.save(commit=False)
            # 指定用户为当前session中存在(已登录)用户
            new_question.author = User.objects.get(id=request.user.id)
            # 所有问题都需要归属于一个板块
            new_question.column = QuestionColumn.objects.get(id=request.POST['column'])
            # 将文章保存至数据库
            new_question.save()
            # 完成后提示发表成功
            messages.success(request, "发表问题成功。")
            # 完成后返回到文章详情
            return redirect(new_question)
        # 数据不合法 ，返回错误信息
        else:
            context = {'is_initial': is_initial}
            messages.error(request, "表单内容有误，请重新填写。")
            return render(request, "question/create_update.html", context)
    # 如果用户请求获取数据
    else:
        context = {'is_initial': is_initial}
        return render(request, 'question/create_update.html', context)


# 更新问题
@login_required(login_url='/userprofile/login/')
def question_update(request, question_id):
    """
    更新文章视图

    判断 权限、用户是否相同 后
    通过POST提交表单，更新title,body字段
    GET方法进入初始界面

    question_id: 问题id
    """
    # 判断该用户是否有相关权限
    if not user_has_permission(request.user.id, 'question.change_questionpost'):
        return HttpResponse('抱歉，您的相关权限已被管理员封禁；如有疑问，请联系管理员！！！')
    # 获取需要修改的具体文章的对象
    question = QuestionPost.objects.get(id=question_id)
    # 只能修改本人的问题
    if request.user != question.author:
        messages.error(request, "抱歉，你无权修改这篇文章")
        return redirect(question)

    is_initial = False  # 用于判断是编辑问题

    # 判断用户是否为 POST提交表单数据
    if request.method == "POST":
        # 将提交的数据保存在表单实例中
        question_post_form = QuestionPostForm(request.POST, request.FILES)

        # 判断提交的数据是否满足模型要求
        if question_post_form.is_valid():
            # 保存新鞋入的title,body
            question_cd = question_post_form.cleaned_data
            question.title = question_cd['title']
            question.description = question_cd['description']

            if request.POST['column'] != 'none':
                question.column = QuestionColumn.objects.get(id=request.POST['column'])
            else:
                question.column = None

            if 'file' in request.FILES:
                question.file = question_cd['file']  # 取文件对象

            question.save()
            # 默认调用question model中的get_absolute_url方法
            return redirect(question)
        else:
            context = {'question': question, 'is_initial': is_initial}
            messages.error(request, "表单内容有误，请重新填写。")
            return render(request, "question/create_update.html", context)
    # 如果用户GET请求获取数据
    else:
        # 赋值上下文 将question对象也穿进去，方便提取旧的内容
        context = {'question': question, 'is_initial': is_initial}
        # 将响应返回到模板中渲染
        return render(request, 'question/create_update.html', context)


@login_required(login_url='/userprofile/login/')
def question_delete(request, question_id):
    """
    删除问题视图

    修改为只能通过POST方式才能成功删除，GET方式或非问题作者用户无法删除文章

    question_id: 问题id
    """
    question = QuestionPost.objects.get(id=question_id)  # 根据id获取需要删掉的问题
    if request.method == "POST":
        if request.user != question.author:
            messages.error(request, "抱歉，你无权删除此问题！")  # 只能删除本人的问题
            return redirect(question)
        else:
            question.delete()  # 调用.delete()方法删除问题
            messages.success(request, "删除问题成功！点击确定跳转首页！")
            return redirect("/")  # 完成后返回主页
    # 其他请求方式无法删除问题
    else:
        messages.error(request, "抱歉，你无法通过此方式删除此问题！")
        return redirect(question)


@login_required(login_url='/userprofile/login/')
def download_file(request, question_id):
    """
    下载附件视图

    判断用户权限后，即可下载问题附件到本地

    question_id: 问题id
    """
    # 判断该用户是否有相关权限
    if not user_has_permission(request.user.id, 'question.view_questionpost'):
        return HttpResponse('抱歉，您的相关权限已被管理员封禁；如有疑问，请联系管理员！！！')

    question = QuestionPost.objects.get(id=question_id)

    filepath = os.path.join(settings.MEDIA_ROOT, str(question.file))
    filename = str(question.file).split("/")[-1]  # 显示在弹出对话框中的默认的下载文件名

    fp = open(filepath, 'rb')
    response = StreamingHttpResponse(fp)
    response['Content-Type'] = 'application/octet-stream'

    response['Content-Disposition'] = 'attachment;filename="{0}"'.format(urlquote(filename.encode('utf8')))
    return response
