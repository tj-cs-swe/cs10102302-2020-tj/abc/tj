from django.urls import path
from . import views

app_name = 'question'

urlpatterns = [

    # 文章详情
    path('question-detail/<int:question_id>/', views.question_detail, name='question_detail'),
    path('question-create/', views.question_create, name='question_create'),
    path('question-update/<int:question_id>/', views.question_update, name='question_update'),
    path('question-delete/<int:question_id>/', views.question_delete, name='question_delete'),
    path('question-download/<int:question_id>', views.download_file, name='download_file'),
]
