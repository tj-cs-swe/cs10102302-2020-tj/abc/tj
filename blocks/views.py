from django.http import HttpResponse,StreamingHttpResponse
from django.shortcuts import render, redirect
import markdown
from comment.models import Comment

from article.models import ArticleColumn  # 文章板块
from article.models import ArticlePost
from article.forms import ArticlePostForm

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from comment import views as comment_views
# 引入评论表单
from comment.forms import CommentForm
# 引入 Q 对象
from django.db.models import Q
from django.conf import settings
from django.utils.http import urlquote
import os
# 引入分页模块
from django.core.paginator import Paginator
#引入权限检查模块
from django.contrib.auth.models import Permission

from django.shortcuts import render, redirect, reverse
from question.models import QuestionPost, QuestionColumn
from question.forms import QuestionPostForm
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.contrib import messages
from answer.models import Answer
from django.http import HttpResponse,StreamingHttpResponse
from django.utils.http import urlquote
from article import views as article_views
from question import views as question_views
# 引入分页模块
import markdown


def sort_column(column_questions, column_articles):
    column_content = []

    for question in column_questions:
        content = {'title': question.title, 'id': question.id, 'column': str(question.column),
                   'updated': question.updated, 'type': 'question'}
        column_content.append(content)
        pass

    for article in column_articles:
        content = {'title': article.title, 'id': article.id, 'column': str(article.column), 'updated': article.updated,
                   'type': 'article'}
        column_content.append(content)
        pass

    column_content = sorted(column_content, key=lambda x: (x['column'], x['updated']), reverse=True)

    # for item in column_content:
    #    if item['column'] != 'None':
    #        print(item)
    return column_content


def column_content(questions, articles,column):
    column_questions = []
    column_articles = []
    for question in questions:
        if question.column and question.column.title == column:
            # print("yes")
            column_questions.append(question)
    for article in articles:
        if article.column and article.column.title == column:
            column_articles.append(article)
    # print(column_articles)
    # print(column_questions)
    return{'questions': column_questions,'articles': column_articles}


def blocks_list3(request,column):
    search = request.GET.get('search')
    if search:
        questions = QuestionPost.objects.filter(
            # 匹配标题(不区分大小写)
            Q(title__icontains=search) |
            # 匹配问题描述(不区分大小写)
            Q(description__icontains=search)
        ).order_by('-updated')
        articles = ArticlePost.objects.filter(
            # 匹配标题(不区分大小写)
            Q(title__icontains=search) |
            # 匹配问题描述(不区分大小写)
            Q(body__icontains=search)
        ).order_by('-updated')
    else:
        questions = QuestionPost.objects.all()
        articles = ArticlePost.objects.all()

    content = column_content(questions, articles, column)
    context = {'questions': content['questions'], 'articles': content['articles'], 'search': search,'title':column}

    return render(request, 'blocks/list3.html', context)
