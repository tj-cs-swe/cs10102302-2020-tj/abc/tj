# 引入path
from django.urls import path
from . import views

# 正在部署的应用的名称
app_name = 'blocks'

urlpatterns = [
    path('blocks-list3/<column>/',views.blocks_list3),
]