from django import forms
from .models import ArticlePost


# 发布文章的表单类
class ArticlePostForm(forms.ModelForm):
    class Meta:
        # 指明数据模型来源
        model = ArticlePost
        # 定义表单包含的字段
        # fields = ('forum', 'title', 'body', 'attachment')
                # 标签，   标题，     正文，     附件
        fields = ('title', 'body', 'attachment')