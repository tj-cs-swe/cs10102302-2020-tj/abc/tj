from django.contrib import admin
from .models import ArticlePost, ArticleColumn

# Register your models here.

admin.site.register(ArticlePost)

# 注册文章板块
admin.site.register(ArticleColumn)