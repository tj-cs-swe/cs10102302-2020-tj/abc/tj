from django.test import TestCase

# Create your tests here.

import datetime
from time import sleep
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from answer.models import Answer
from question.models import QuestionPost
from collect.models import CollectRecord
from like.models import LikeRecord


class AnswerModelTests(TestCase):

    def test_was_created_recently_with_future_answer(self):
        # 若回答创建时间为为未来，则返回 FALSE
        author = User(username='author', password='test_password')
        author.save()
        user = User(username='user', password='test_password')
        user.save()

        question = QuestionPost(
            author = author,
            column = None,
            forum = None,
            title = 'test_title',
            description = 'test_question'
        )
        question.save()

        future_answer = Answer(
            question = question,
            user = user,
            body = 'test',
            created = timezone.now() + datetime.timedelta(days=30)
        )

        self.assertIs(future_answer.was_created_recently(),False)
        pass


    def test_was_created_recently_with_seconds_before_answer(self):
        # 回答创建时间为 1 分钟内，返回TRUE
        author = User(username='author1', password='test_password')
        author.save()
        user = User(username='user1', password='test_password')
        user.save()
        
        
        question = QuestionPost(
            author = author,
            column = None,
            forum = None,
            title = 'test_title1',
            description = 'test_question1'
        )
        question.save()

        seconds_before_answer = Answer(
            question = question,
            user = user,
            body='test_1',
            created=timezone.now() - datetime.timedelta(seconds=45)
        )

        self.assertIs(seconds_before_answer.was_created_recently(),True)
        pass


    def test_was_created_recently_with_days_before_article(self):
        # 回答创建时间为若干天内，返回FALSE
        author = User(username='author2', password='test_password')
        author.save()
        user = User(username='user2', password='test_password')
        user.save()
        
        question = QuestionPost(
            author = author,
            column = None,
            forum = None,
            title = 'test_title2',
            description = 'test_question2'
        )
        question.save()

        seconds_before_answer = Answer(
            question=question,
            user = user,
            body='test_2',
            created=timezone.now() - datetime.timedelta(days=30)
        )

        self.assertIs(seconds_before_answer.was_created_recently(), False)
        pass


    def test_answer_body(self):
        # 检查回答内容
        author = User(username='author3', password='test_password')
        author.save()
        user = User(username='user3', password='test_password')
        user.save()

        question = QuestionPost(
            author = author,
            column = None,
            forum = None,
            title = 'test_title3',
            description = 'test_question3',
        )
        question.save()
        
        body = 'test_3'
        answer = Answer(
            question = question,
            user = user,
            body = body
        )

        self.assertIs(answer.__str__(), body[:20])
        pass


    def test_get_absolute_url(self):
        # 检查回答url
        author = User(username='author4', password='test_password')
        author.save()
        user = User(username='user4', password='test_password')
        user.save()

        question = QuestionPost(
            author = author,
            column = None,
            forum = None,
            title = 'test_title4',
            description = 'test_question4',
        )
        question.save()

        answer = Answer(
            question = question,
            user = user,
            body = 'test_4'
        )
        answer.save()

        self.assertEqual(answer.get_absolute_url(), reverse('question:question_detail', args=[answer.question.id, ]))
        pass


    def test_question_is_collected(self):
        # 刚创建一个回答，没有被收藏，返回False，若创建一条收藏记录后，则返回True
        author = User(username='author5', password='test_password')
        author.save()
        user = User(username='user5', password='test_password')
        user.save()
        title = 'test_title5'

        question = QuestionPost(
            author = author,
            column = None,
            forum = None,
            title = title,
            description = 'test_question5',
        )
        question.save()

        answer = Answer(
            question = question,
            user = user,
            body = 'test_5'
        )
        answer.save()

        self.assertIs(answer.answer_is_collected(author), False)

        record = CollectRecord(
            content_type = ContentType.objects.get_for_model(Answer),
            object_id = answer.id,
            user = author,
            collected_time = timezone.now()
        )

        record.save()
        sleep(0.5)

        self.assertIs(answer.answer_is_collected(author), True)
        pass


    def test_question_is_liked(self):
        # 刚创建一个问题，没有被收藏，返回False，若创建一条收藏记录后，则返回True
        author = User(username='author6', password='test_password')
        author.save()
        user = User(username='user6', password='test_password')
        user.save()

        title = 'test_title6'
        question = QuestionPost(
            author = author,
            column = None,
            forum = None,
            title = title,
            description = 'test_question6',
        )
        question.save()

        answer = Answer(
            question = question,
            user = user,
            body = 'test_6'
        )
        answer.save()

        self.assertIs(answer.answer_is_liked(author), False)

        record = LikeRecord(
            content_type = ContentType.objects.get_for_model(Answer),
            object_id = answer.id,
            user = author,
            liked_time = timezone.now()
        )

        record.save()
        sleep(0.5)
        self.assertIs(answer.answer_is_liked(author), True)
        pass

    pass


class AnswerViewTest(TestCase):
    
    pass