from django.urls import path
from . import views

app_name = 'answer'

urlpatterns = [
    path('answer-create/<int:question_id>/', views.answer_create, name='answer_create'),
    path('answer-update/<int:question_id>/<int:answer_id>/', views.answer_update, name='answer_update'),
    path('answer-delete/<int:question_id>/<int:answer_id>/', views.answer_delete, name='answer_delete'),
    path('answer-download/<int:id>/', views.download_file, name='download_file'),
]
